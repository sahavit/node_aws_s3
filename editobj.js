var AWS = require('aws-sdk');
var fs = require('fs')

var awsConfig = { 
    "accessKeyId": "<id>",
    "secretAccessKey": "<key>",
    "region": "ap-south-1" 
}

var s3 = new AWS.S3(awsConfig);

//function to add the edited file object to the bucket
var editObject = function(){

    var params = {
        ACL:'public-read',
        Body: fs.readFileSync('./assets/textdata.txt'),
        Bucket: 'mobiotics.assn.1',
        Key: "textdata.txt",
        ContentType: 'text/plain'
    }

    s3.putObject(params, function(err, data) {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("Object Edited Successfully!", data);
        }
    });
}

editObject();