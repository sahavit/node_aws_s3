var AWS = require('aws-sdk');

var awsConfig = { 
    "accessKeyId": "<id>",
    "secretAccessKey": "<key>",
    "region": "ap-south-1" 
}

var s3 = new AWS.S3(awsConfig);

//function to delete the given object in bucket
var deleteObject = function(){

    var params = {
        Bucket: 'mobiotics.assn.1',
        Key: "nature-3.jpg"
    }

    s3.deleteObject(params, function(err, data) {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("Object Deleted Successfully!");
        }
    });
}

deleteObject();