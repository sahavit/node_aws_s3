var AWS = require('aws-sdk');

var awsConfig = { 
    "accessKeyId": "<id>",
    "secretAccessKey": "<key>",
    "region": "ap-south-1" 
}

var s3 = new AWS.S3(awsConfig);
var cloudfront = new AWS.CloudFront(awsConfig);

//function to list all buckets
var getBuckets = function(){
    
    s3.listBuckets(function(err, data) {
        if (err) {
          console.log("Error", err);
        } else {
            console.log("Buckets:", data.Buckets);
        }
    });
}

//function to get domain details from cloudfront
var listDistribution = new Promise( function(res,rej){

  var params ={
      Id: '<id>'
  }
  cloudfront.getDistribution(params, function(err, data) {
      if (err) {
        console.log("Error", err);
        rej(err)
      } 
      else {
        res(data.Distribution.DomainName)
      }
  })
})

//function to fetch all objects in the given bucket and their complete domain URLs
var getObjects = function(){

  console.log("Fetching data....")

  var params = {
      Bucket: 'mobiotics.assn.1'
  }

  function printDetails(data,domain){
    data.Contents.forEach(element => {
      element.ObjectUrl=domain+'/'+element.Key
    });
      console.log("Objects:",data.Contents)
  }

  s3.listObjects(params, async function(err, data) {
      if (err) {
        console.log("Error", err);
      } 
      else {
        var domain = await listDistribution;
        printDetails(data,domain);
      }
  });
}

//calling functions
getBuckets();
getObjects();