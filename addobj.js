var AWS = require('aws-sdk');
var fs = require('fs');

var awsConfig = { 
    "accessKeyId": "<id>",
    "secretAccessKey": "<key>",
    "region": "ap-south-1" 
}

var s3 = new AWS.S3(awsConfig);

//function to add given object to the bucket
var addObject = function(){

    var params = {
        ACL:'public-read',
        Body: fs.readFileSync('./assets/nature-3.jpg'),
        Bucket: 'mobiotics.assn.1',
        Key: "nature-3.jpg",
        ContentType: 'image/jpeg'
    }

    s3.putObject(params, function(err, data) {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("Object Added Successfully!", data);
        }
    });
}

addObject();